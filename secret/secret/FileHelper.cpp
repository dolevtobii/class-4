#include "FileHelper.h"
#include <iostream>
#include <string>
#include <fstream>

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::fstream;
using std::getline;

/*
	function will convert file to string
	input: filename- the file name to read
	output the file as string
*/
string FileHelper::readFileToString(string fileName)
{
	string line = "";
	string finalStr = "";
	fstream file;
	file.open(fileName, fstream::in);

	if (file.is_open())
	{
		while (getline(file, line))
		{
			finalStr += line;
			finalStr += "\n";
		}
		file.close();
	}
	else
	{
		cout << "file not found!" << endl;
	}

	return finalStr;
}


/*
	function will write a file to file word by word
	input: inputFileName- the input file name
		   outputFileName- the output file name
	output:none
*/
void FileHelper::writeWordsToFile(string inputFileName, string outputFileName)
{
	string word = "";
	fstream inFile;
	fstream outFile;
	inFile.open(inputFileName, fstream::in);
	outFile.open(outputFileName,fstream::out);

	if (inFile.is_open())
	{
		while (inFile >> word)
		{
			outFile << word << endl;
		}
	}
	else
	{
		cout << "cant open input file!" << endl;
	}

	inFile.close();
	outFile.close();
}

