#include "PlainText.h"
#include <iostream>
#include <string>

using std::cout;
using std::cin;
using std::endl;
using std::string;

int PlainText::_NumOfTexts = 0;


/*
	function will build the class for work
	input: text- the text encrypt
	output:none
*/
PlainText::PlainText(string text)
{
	this->_text = text;
	this->_isEncrypted = false;
	_NumOfTexts++;
}


/*
	function will destoyed the class and delete memory
	input: none
	output: none
*/
PlainText::~PlainText()
{
	
}


/*
	function will return if the text is Encrypted
	input: none
	output: if the text is Encrypted
*/
bool PlainText::isEnc()
{
	return this->_isEncrypted;
}


/*
	function will return the text
	input: none
	output: the text
*/
string PlainText::getText()
{
	return this->_text;
}


/*
	function will print the text and method
	input: os- the os stream
		   p- a refrence to the class
	output: the os stream to print
*/
std::ostream& operator<<(std::ostream& os, const PlainText& p)
{
	os << "PlainText\n" << p._text << endl;
	return os;
}


//--------------------------------------------------------------
//my function
/*
	function will return the number of text we encrypted
	input: none
	output: the number
*/
int PlainText::get_Num_Of_Texts()
{
	return _NumOfTexts;
}
