#pragma once
#include <iostream>
#include <string>
#include "PlainText.h"

using std::string;

class SubstitutionText : public PlainText
{
public:
	SubstitutionText(string text, string dictionaryFileName);
	~SubstitutionText();
	string encrypt();
	string decrypt();
	friend std::ostream& operator<<(std::ostream& os, const SubstitutionText& s);

private:
	string _dictionaryFileName;
};

