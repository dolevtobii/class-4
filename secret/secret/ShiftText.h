#pragma once
#include <iostream>
#include <string>
#include "PlainText.h"

using std::string;

class ShiftText : public PlainText
{
public:
	ShiftText(string text, int key);
	~ShiftText();
	string encrypt();
	string decrypt();
	friend std::ostream& operator<<(std::ostream& os, const ShiftText& s);


protected:
	int _key;

};