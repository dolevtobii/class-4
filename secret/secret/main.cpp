#include <iostream>
#include <string>
#include "PlainText.h"
#include "ShiftText.h"
#include "CaesarText.h"
#include "SubstitutionText.h"
#include "FileHelper.h"

using std::cout;
using std::cin;
using std::endl;
using std::string;

#define DECRYPTION_STRING 1
#define DECRYPTION_FILE 2
#define PRINT_TO_SCREEN 2
#define	TEXT_COUNT_PRINT 3
#define EXIT 4


int main()
{
	PlainText plain("nothing");
	FileHelper fileH;

	int userChoice = 0;
	int FileOrScreen = 0;
	int key = 0;

	string dictionaryFileName = "";
	string outputFileName = "";
	string fileName = "";
	string codeStr = "";
	string encryptionMethod = "";
	
	do
	{
		cout << "\n1- Encrypt and decrypt a string \n2- Encrypt and decrypt a file \n3- Print the number of texts we decrypted \n4- Exit" << endl;
		cout << "Enter your choice number: ";
		cin >> userChoice;
		cout << "\n";

		switch (userChoice)
		{
		case DECRYPTION_STRING:
			cout << "Enter a string to decrypt: ";
			cin >> codeStr;
			cout << "\n";

			cout << "Enter encryption method: ";
			cin >> encryptionMethod;
			cout << "\n";

			
			if (encryptionMethod == "Shift")
			{
				cout << "Enter key: ";
				cin >> key;
				cout << "\n";

				ShiftText shift(codeStr, key);
				shift.decrypt();
				shift.decrypt();

				cout << shift << endl;
			}
			else if (encryptionMethod == "Caesar")
			{
				CaesarText caesar(codeStr);
				caesar.decrypt();
				caesar.decrypt();

				cout << caesar << endl;
			}
			else if (encryptionMethod == "Substitution")
			{
				cout << "Enter dictionary file name: ";
				cin >> dictionaryFileName;
				cout << "\n";
				SubstitutionText substitution(codeStr, dictionaryFileName);
				substitution.decrypt();
				substitution.decrypt();

				cout << substitution << endl;
			}
			else
			{
				cout << "wrong encryption method! try again pls" << endl;
			}

			break;

		case DECRYPTION_FILE:
			cout << "Enter a file(.txt) to decrypt: ";
			cin >> fileName;
			cout << "\n";

			codeStr = fileH.readFileToString(fileName);

			cout << "1- write to new file \n2- write on screen" << endl;
			cin >> FileOrScreen;
			cout << "\n";

			if (encryptionMethod == "Shift")
			{
				cout << "Enter key: ";
				cin >> key;
				cout << "\n";

				ShiftText shift(codeStr, key);
				shift.decrypt();
				shift.decrypt();

				if(FileOrScreen == 1)
				{
					cout << "Enter out put file name: ";
					cin >> outputFileName;
					cout << "\n";

					fileH.writeWordsToFile(fileName, outputFileName);
				}
				else if (FileOrScreen == PRINT_TO_SCREEN)
				{
					cout << shift << endl;
				}
				else 
				{
					cout << "you choose nothing from above..." << endl;
				}
			}
			else if (encryptionMethod == "Caesar")
			{
				CaesarText caesar(codeStr);
				caesar.decrypt();
				caesar.decrypt();

				if (FileOrScreen == 1)
				{
					cout << "Enter out put file name: ";
					cin >> outputFileName;
					cout << "\n";

					fileH.writeWordsToFile(fileName, outputFileName);
				}
				else if (FileOrScreen == PRINT_TO_SCREEN)
				{
					cout << caesar << endl;
				}
				else
				{
					cout << "you choose nothing from above..." << endl;
				}
			}
			else if (encryptionMethod == "Substitution")
			{
				cout << "Enter dictionary file name: ";
				cin >> dictionaryFileName;
				cout << "\n";
				SubstitutionText substitution(codeStr, dictionaryFileName);
				substitution.decrypt();
				substitution.decrypt();

				if (FileOrScreen == 1)
				{
					cout << "Enter out put file name: ";
					cin >> outputFileName;
					cout << "\n";

					fileH.writeWordsToFile(fileName, outputFileName);
				}
				else if (FileOrScreen == PRINT_TO_SCREEN)
				{
					cout << substitution << endl;
				}
				else
				{
					cout << "you choose nothing from above..." << endl;
				}
			}
			else
			{
				cout << "wrong encryption method! try again pls" << endl;
			}
			break;

		case TEXT_COUNT_PRINT:
			cout << "the number of times you decrypted: ";
			cout << plain.get_Num_Of_Texts() - 1<< endl;
			break;

		case EXIT:
			cout << "GoodBye!" << endl;
			break;

		default:
			cout << "number not found on the menu! try again pls." << endl;
			
		}
	
	} while (userChoice != EXIT);



	if (_CrtDumpMemoryLeaks())
	{
		std::cout << "Memory leaks!\n";
	}
	else
	{
		std::cout << "No leaks\n";
	}
	system("pause");
	return 0;
}

