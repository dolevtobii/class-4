#include "SubstitutionText.h"
#include <iostream>
#include <string>
#include <fstream>


using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::fstream;
using std::getline;

#define ENCRYPT_INDEX 2

/*
	function will build the class for work
	input: text- the text encrypt
		   dictionaryFileName- the dictionary file link
	output:none
*/
SubstitutionText::SubstitutionText(string text, string dictionaryFileName) : PlainText(text)
{
	this->_text = text;
	this->_dictionaryFileName = dictionaryFileName;
	this->encrypt();
}


/*
	function will destoyed the class and delete memory
	input: none
	output: none
*/
SubstitutionText::~SubstitutionText()
{

}


/*
	function will encrypt the text by the dictionary
	input: none
	output: the text encrypted
*/
string SubstitutionText::encrypt()
{
	int i = 0;
	bool flag = true;
	string line = "";
	fstream file;

	file.open(this->_dictionaryFileName, fstream::in);
	if (file.is_open())
	{
		for (i = 0; i < this->_text.length(); i++)
		{
			while (getline(file, line) && flag == true)
			{
				if ((line[0] == this->_text[i]) && (this->_text[i] >= 'a' && this->_text[i] <= 'z'))
				{
					this->_text[i] = line[ENCRYPT_INDEX];
					flag = false;
				}
			}
			file.clear();
			file.seekg(0);
			flag = true;
		}
		file.close();
	}
	else
	{
		cout << "file not found!" << endl;
	}

	this->_isEncrypted = true;
	return this->_text;
}


/*
	function will decrypt the text by the dictionary
	input: none
	output: the text decrypted
*/
string SubstitutionText::decrypt()
{
	int i = 0;
	bool flag = true;
	string line = "";
	fstream file;

	file.open(this->_dictionaryFileName, fstream::in);
	if (file.is_open())
	{
		for (i = 0; i < this->_text.length(); i++)
		{
			while (getline(file, line) && flag == true)
			{
				if ((line[ENCRYPT_INDEX] == this->_text[i]) && (this->_text[i] >= 'a' && this->_text[i] <= 'z'))
				{
					this->_text[i] = line[0];
					flag = false;
				}
			}
			file.clear();
			file.seekg(0);
			flag = true;
		}
		file.close();
	}
	else
	{
		cout << "file not found!" << endl;
	}

	this->_isEncrypted = true;
	return this->_text;
}


/*
	function will print the encryped text and method
	input: os- the os stream
		   s- a refrence to the class
	output: the os stream to print
*/
std::ostream& operator<<(std::ostream& os, const SubstitutionText& s)
{
	os << "SubstitutionText\n" << s._text << endl;
	return os;
}
