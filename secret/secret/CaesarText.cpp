#include "CaesarText.h"
#include "ShiftText.h"
#include <iostream>
#include <string>

using std::cout;
using std::cin;
using std::endl;
using std::string;

#define CAESAR_KEY 3

/*
	function will build the class for work
	input: text- the text encrypt
		   key- the encrypt key
	output:none
*/
CaesarText::CaesarText(string text) : ShiftText(text, 3)
{
	this->_text = text;
	this->_key = CAESAR_KEY;
	this->encrypt();
}


/*
	function will destoyed the class and delete memory
	input: none
	output: none
*/
CaesarText::~CaesarText()
{

}


/*
	function will decrypt the text by the key
	input: none
	output: the text decrypted
*/
string CaesarText::decrypt()
{
	return ShiftText::decrypt();
}


/*
	function will print the encryped text and method
	input: os- the os stream
		   ca- a refrence to the class
	output: the os stream to print
*/
std::ostream& operator<<(std::ostream& os, const CaesarText& ca)
{
	os << "Caesar\n" << ca._text << endl;
	return os;
}