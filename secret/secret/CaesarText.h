#pragma once
#include <iostream>
#include <string>
#include "ShiftText.h"

using std::string;

class CaesarText : public ShiftText
{
public:
	CaesarText(string text);
	~CaesarText();
	string decrypt();
	friend std::ostream& operator<<(std::ostream& os, const CaesarText& ca);
};

