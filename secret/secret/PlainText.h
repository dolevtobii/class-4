#pragma once
#include <iostream>
#include <string>

using std::string;

class PlainText
{
public:
	PlainText(string text);
	~PlainText();
	bool isEnc();
	string getText();
	friend std::ostream& operator<<(std::ostream& os, const PlainText& p);

	//my functions
	int get_Num_Of_Texts();

protected:
	string _text;
	bool _isEncrypted;

	static int _NumOfTexts;
};

