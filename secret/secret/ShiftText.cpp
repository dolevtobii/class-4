#include "ShiftText.h"
#include <iostream>
#include <string>

using std::cout;
using std::cin;
using std::endl;
using std::string;


/*
	function will build the class for work
	input: text- the text encrypt
		   key- the encrypt key
	output:none
*/
ShiftText::ShiftText(string text, int key) : PlainText(text)
{
	this->_text = text;
	this->_key = key;
	this->encrypt();
}


/*
	function will destoyed the class and delete memory
	input: none
	output: none
*/
ShiftText::~ShiftText()
{
	
}


/*
	function will encrypt the text by the key 
	input: none
	output: the text encrypted
*/
string ShiftText::encrypt()
{
	int i = 0;

	for (i = 0; i < this->_text.length(); i++)
	{
		if(this->_text[i] >= 'a' && this->_text[i] <= 'z')
		{
			this->_text[i] += this->_key;
		}
	}

	this->_isEncrypted = true;
	return this->_text;
}


/*
	function will decrypt the text by the key
	input: none
	output: the text decrypted
*/
string ShiftText::decrypt()
{
	int i = 0;

	for (i = 0; i < this->_text.length(); i++)
	{
		if (this->_text[i] >= 'a' && this->_text[i] <= 'z')
		{
			this->_text[i] -= this->_key;
		}
	}

	this->_isEncrypted = false;
	return this->_text;
}


/*
	function will print the encryped text and method
	input: os- the os stream
		   s- a refrence to the class
	output: the os stream to print
*/
std::ostream& operator<<(std::ostream& os, const ShiftText& s)
{
	os << "ShiftText\n" << s._text << endl;
	return os;
}
